package common

import (
	"crypto/sha256"
	"database/sql"
	"database/sql/driver"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"time"

	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

/*
User 定义usr结构体
用户的主要信息有：用户名、密码、用户角色/权限、用户冻结时期字段、有效期字段、开始登陆时间、连续登录次数
具体解释如下：
- name:用户名，string，用户的昵称，也需要同其他用户不重复；
- passwd:密码，设置8-20位字符串，采用SHA256加密方式加密后存储于数据库内；
- role:用户角色，若为1，则为管理员角色，若为0，则为普通用户，用于区分不同角色；
- release_time:冻结期字段，用于限制用户在输入密码错误次数超过3次后，才更新该字段，若当前时间小于有效期字段，则视为超出使用时间范围，则该用户不可登录；
- deadline_time:有效期字段，用于限制用户的使用时长，若当前时间大于有效期字段，则视为超出使用时间范围，则该用户不可登录；
- login_start_time:开始登录时间，该字段用于限定该用户每日开始登录的时间，只有当前时间大于该值时，该用户才可登录；
- login_end_time:结束登录时间，该字段用于限定该用户每日结束登录的时间，只有当前时间小于该值时，该用户才可登录；
- fail_login_times:该用户连续登录失败次数，该次数超过3触发用户冻结选项。
*/
type User struct {
	gorm.Model
	Name         string `gorm:"type:varchar(20);not null;unique" json:"name" `
	Passwd       string `gorm:"size(255);not null" json:"passwd"`
	Role         int    `gorm:"type:int;not null" json:"role"`
	ReleaseTime  int64  `gorm:"type:int;not null" json:"releaseTime"`
	DeadlineTime int64  `gorm:"type:int;not null" json:"deadlineTime"`
	LoginStart   int64  `gorm:"type:int;not null" json:"loginStart"`
	LoginEnd     int64  `gorm:"type:int;not null" json:"loginEnd"`
	FailTimes    int    `gorm:"type:int;not null" json:"failTimes"`
}

type inferClass []string //自定义gorm的数据类型，就是为了让gorm能够存储[]string类型的数据，把它作为json格式字符串存储

// Scan implements the Scanner interface.从数据库中取出来数据的时候会调用这个方法，将其转换为interface{},然后再转换为inferClass
func (t *inferClass) Scan(value interface{}) error {
	bytesValue, _ := value.([]byte)
	return json.Unmarshal(bytesValue, t)
}

// Value implements the driver Valuer interface.存入数据库的时候会调用这个方法，将其转换为json格式的[]byte
func (t inferClass) Value() (driver.Value, error) {
	jsonValue, err := json.Marshal(t)
	return string(jsonValue), err
}

// Camera 定义camera结构体
/*
| 字段名称    | 字段  类型 | 字段  默认值 | 是否允  许为空 | 是否  主键 | 示例              |
| ----------- | ---------- | ------------ | -------------- | ---------- | ----------------- |
| ca_id       | int        |              | 否             | 是         | 1                 |
| name        | string     |              | 否             |            | “Camera_01”       |
| ip          | string     |              | 否             |            | “192.168.1.64”    |
| port        | string     | “554”        | 否             |            | “554”             |
| channel     | int        | 1            | 否             |            | 1                 |
| user        | string     |              | 否             |            | “admin”           |
| passwd      | string     |              | 否             |            | “admin123”        |
| area        | string     |              | 否             |            | “猛追湾”          |
| startTime  | string     | “0:00:00”    | 否             |            | “06:00:00”        |
| endTime   | string     | “23:59:59”   | 否             |            | “23:00:00”        |
| inferClass | list       |              | 否             |            | [“fight”,“drunk”] |
*/
type Camera struct {
	gorm.Model        //主键
	Name       string `gorm:"type:varchar(20);not null;unique" json:"name"` //摄像头名字
	Ip         string `gorm:"type:varchar(30);not null" json:"ip"`          //摄像头网络ip
	Port       string `gorm:"type:varchar(20);not null" json:"port"`        //摄像头网络port
	Channel    int64  `gorm:"type:varchar(20);not null" json:"channel"`     //该字段为访问摄像头中一个必不可少的字段，不同的channel意味着这台监控主机所显示的不同画面
	User       string `gorm:"type:varchar(20);not null" json:"user"`        //该字段为登录该摄像头的用户名字段
	Passwd     string `gorm:"type:varchar(30);not null" json:"passwd"`      //该字段为登录该摄像头的密码字段
	Area       string `gorm:"type:varchar(40);not null" json:"area"`        //区域，限定该摄像头来自哪个辖区内
	StartTime  int64  `gorm:"type:int;not null" json:"startTime"`           //该摄像头开始检测时间
	EndTime    int64  `gorm:"type:int;not null" json:"endTime"`             //该摄像头结束检测时间
	//这里我们的InferClass应该是一个数组，但是mysql不支持数组的格式，所以我们需要做一些调整，思路如下：
	//第一步将InferClass []string类型编码为JSON类型数据；第二步自定义JSON类型数据，使得gorm可以支持，可以参考GORM-自定义数据类型(https://link.juejin.cn/?target=https%3A%2F%2Fgorm.io%2Fzh_CN%2Fdocs%2Fdata_types.html)
	InferClass inferClass `gorm:"type:varchar(40)" json:"inferClass"` //该摄像头检测类别列表,是一个字符串数组
}

/*
Alert 报警信息表Alert
- a_id:主键，int，标识报警信息的唯一性，顺序递增；
- ca_id:摄像头id，用于标识报警信息来自于哪一个摄像头；
- alert_time:报警时间，标识报警事件发生的时间；
- alerttype:报警类型，即标识不同的触发报警的事件类型；
- pathPhoto:该字段存储的是报警图片在服务器中的存储路径。
- pathVideo:该字段存储的是报警视频在服务器中的存储路径。
*/
type Alert struct {
	gorm.Model
	CaId      int    `gorm:"type:int;not null" json:"caId"`
	AlertTime int64  `gorm:"type:int;not null" json:"alertTime"` //报警时间，标识报警事件发生的时间；为一个unix时间戳
	AlertType string `gorm:"type:varchar(20);not null" json:"alertType"`
	PathPhoto string `gorm:"type:varchar(150);not null" json:"pathPhoto"` //报警图片的存储地址,因为是绝对路径存储，可能会比较长
	PathVideo string `gorm:"type:varchar(150);not null" json:"pathVideo"` //报警视频的存储地址,因为是绝对路径存储，可能会比较长
}

/*
Box 检测区域框表Box
- b_id:主键，int，用来标识检测区域框信息的唯一性；
- ca_id:摄像头id，用于标识报警信息来自于哪一个摄像头；
- left_up:每一条矩形框数据左上角的点“x y”所占整个图像（w，h）的比例；
- right_down:每一条矩形框数据右下角的点“x y”所占整个图像（w，h）的比例。
*/
type Box struct {
	gorm.Model
	CaId      int    `gorm:"type:int;not null" json:"caId"` //摄像头id，用于标识报警信息来自于哪一个摄像头；
	LeftUp    string `gorm:"type:varchar(20);not null" json:"leftUp"`
	RightDown string `gorm:"type:varchar(20);not null" json:"rightDown"`
}

func InitDB() {
	//从配置文件中读取host、port、username、passwd这些与数据库相关的信息
	host := viper.GetString("database.host")
	port := viper.GetString("database.port")
	driverName := viper.GetString("database.driverName")
	username := viper.GetString("database.username")
	passwd := viper.GetString("database.passwd")
	charset := viper.GetString("database.charset")
	database := viper.GetString("database.database")
	rootUser := viper.GetString("database.rootUser")
	rootPasswd := viper.GetString("database.rootPasswd")
	// 连接 MySQL 数据库服务器
	dbRaw, err := sql.Open(driverName, username+":"+passwd+"@tcp("+host+":"+port+")/")
	if err != nil {
		log.Fatal("database connect err: " + err.Error())
	}
	// 创建新的数据库
	_, err = dbRaw.Exec("CREATE DATABASE " + database)
	if err != nil {
		log.Println("database create err: " + err.Error())
	}
	err = dbRaw.Close()
	if err != nil {
		log.Println("database close err: " + err.Error())
		return
	}
	// 格式化 DSN 字符串
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=Local", username, passwd, host, port, database, charset)
	// 连接到 MySQL 数据库服务器
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	// 自动迁移,用户表
	err = db.AutoMigrate(&User{})
	if err != nil {
		log.Fatal(err)
	}
	//摄像头表
	err = db.AutoMigrate(&Camera{})
	if err != nil {
		log.Fatal(err)
	}
	//报警信息表Alert
	err = db.AutoMigrate(&Alert{})
	if err != nil {
		log.Fatal(err)
	}
	//检测区域框表Box
	err = db.AutoMigrate(&Box{})
	if err != nil {
		log.Fatal(err)
	}
	//在用户表里面插入root用户，如果root用户已经存在就不插入了
	var rootUsr User
	err = db.Where("name = ?", rootUser).First(&rootUsr).Error
	if err == gorm.ErrRecordNotFound || rootUsr.ID == 0 { //如果root用户不存在,则插入root用户
		//密码加密,使用sha256加密
		h := sha256.New()
		h.Write([]byte(rootPasswd))
		sha256Passwd := hex.EncodeToString(h.Sum(nil))
		//插入root用户
		rootUsr = User{
			Name:   rootUser,
			Passwd: sha256Passwd,
			Role:   1, //root用户的权限为1
			//设置loginStart为8:35,存的是unix时间戳的8小时35分钟的秒数
			LoginStart: int64(8*time.Hour + 35*time.Minute),
			//设置loginEnd为20:35,存的是unix时间戳的20小时35分钟的秒数
			LoginEnd: int64(20*time.Hour + 35*time.Minute),
			//设置有效时间，deadlineTime设置为最大时间，对于管理员来说，有效时间是无限的
			DeadlineTime: int64(math.MaxInt64),
			//初始化冻结时间，最开始未被冻结，所以设置为0
			ReleaseTime: 0,
		}
		//将root用户插入到数据库中
		err = db.Create(&rootUsr).Error
	}
	if err != nil {
		log.Fatal(err)
	}
	DB = db //将db赋值给全局变量DB
	log.Println("database init success")
}
