package common

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

//从application中得到jwt的秘钥

var jwtKey = []byte(viper.GetString("settings.jwt_key"))

type Claims struct {
	Id   uint
	Role int //0为普通用户，1为管理员
	jwt.StandardClaims
}

//type UserDto struct {
//	Name string `json:"name"` //用户名
//	Id   uint   `json:"id"`   //用户id
//	Role int    `json:"role"` //0为普通用户，1为管理员
//}
//
//func ToUserDto(user *User) UserDto {
//	return UserDto{
//		Name: user.Name,
//		Id:   user.ID,
//		Role: user.Role,
//	}
//}

// GetToken 生成jwt的token
func GetToken(user User) (string, error) {
	//从application中获取token有效时长
	validationTime := time.Duration(viper.GetInt64("settings.validation_time"))
	expirationTime := time.Now().Add(validationTime * 24 * time.Hour) //设置过期时间
	claims := &Claims{
		Id:   user.ID,
		Role: user.Role,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	if tokenString, err := token.SignedString(jwtKey); err != nil {
		return "", err
	} else {
		return tokenString, nil
	}
}

// ParseToken 从tokenString中解析出相关信息
func ParseToken(tokenString string) (*jwt.Token, *Claims, error) {
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString, claims,
		func(token *jwt.Token) (i interface{}, err error) {
			return jwtKey, nil
		})
	return token, claims, err
}

// AuthMiddleware 验证解析token
// @Description: 验证解析token
// @Author liamY
func AuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		//获取authorization header
		tokenString := ctx.GetHeader("Authorization")

		//验证token格式,若token为空，则token格式不对
		if tokenString == "" {
			ctx.JSON(http.StatusUnauthorized, gin.H{"code": 401, "msg": "没有Authorization字段"})
			ctx.Abort() //将此次请求抛弃
			return
		}

		//从tokenString中解析信息
		token, claims, err := ParseToken(tokenString)
		if err != nil || !token.Valid {
			ctx.JSON(http.StatusUnauthorized, gin.H{"code": 401, "msg": "jwt的token无效"})
			ctx.Abort()
			return
		}
		// 查询tokenString中的user信息是否存在
		userId := claims.Id
		db := DB
		var user User
		db.First(&user, userId) //从user表里面查询id为userId的用户
		if user.ID == 0 {
			ctx.JSON(http.StatusUnauthorized, gin.H{"code": 401, "msg": "用户不存在"})
			ctx.Abort()
			return
		}
		//若存在该用户则将用户信息写入上下文
		//后面可以通过ctx.Get("user")来获取当前请求的用户信息
		ctx.Set("user", user)
		ctx.Next()
	}
}
