# 后端任务板块

相信你是从前端板块过来的呀，如果不是，要么你是有基础或者你是看错了不知道应该先完成前端的板块。我们这里是与机器信息后端交互，返还给后端需要的摄像头信息，比如摄像头列表，还有摄像头检测区域时间类别，并且很重要的是，会得到后端发送过来的报警信息，检测到嫌疑犯了！

## task4摄像头业务

任务中的接口都定义在src/mlBackend/transaction/getInfo.go(这里取名字稍微有点不合理)

### 4.1实现所有摄像头检测区域、时间、类别接口（这只是一个接口，一个函数）

对应在apifox里面有写

这次我会删掉代码里面的一些部分让你填空（轻松一些啦），你需要按照apifox上的定义形式以及参考响应示填出答案。

（为了最开始代码跑的通，我把整个函数注释掉了，记得填空的时候把函数注释去掉哦），而且还要注意一下routes.go这个文件是不是也应该有点调整呢？

### 4.2假的任务

这个任务不需要写代码，只是让你思考，apifox里面明明有两个![image-20230802173048431](https://fastly.jsdelivr.net/gh/KMSorSMS/picGallery/img/202308021751535.png)接口是摄像头的业务，但是我说那个摄像头列表任务不需要做，只要我们用原本在前端的  “摄像头获得的接口“就好了，你可以想想是不是，并且说一下routes.go里面需要做什么修改

## task5报警业务

任务中的接口都定义在src/mlBackend/transaction/alertTransaction.go里面

这里我们是要获得后端发送过来的报警信息，我们将报警信息部分存入数据库的alert表中，由于同一个报警信息有两次发送，第一次发送图片，第二次发送视频，至于这是为什么，就让我把前面前端部分的README的内容复制过来解释一下：

> 后端机器学习部分检测到嫌疑犯，就会向服务器发送检测到嫌疑犯的图片，以及是哪个摄像头（ca_id）在什么时间(alert_time)检测到嫌疑犯是什么类型的犯罪（type），只要是ca_id alert_time type组合起来是不同的，就说明这是一条不同的报警信息，就不算做重复，需要按照alertType/alertTimeString-caIdString.jpg这样的格式存储到photoSource路径下面，而这个时候报警视频还没有传过来，因为后端还会等一会儿，所以这里会分成两个接口![image-20230802155650824](https://fastly.jsdelivr.net/gh/KMSorSMS/picGallery/img/202308021620539.png)对应着不同的环节，后面报警视频上传的时候，我们从apifox里面也能看到，警报信息和视频上传的请求格式都是form-data了，因为这样能直接上传二进制文件，然后我们要先检查报警视频对应的报警信息部分是否是已经存在的，因为必须要存在你这个视频才能传输上去，然后在再接收这个报警视频，并且更新数据库表的信息。

### 5.1实现报警信息上传接口---AlertInfo

这里我们的难度会有提升，需要你续写代码，我的代码写到接收完请求参数的位置就结束了，剩下的将报警信息存到数据库，将报警图片存到服务器本地需要你自己完成，这里给一点存储到本地可以使用的函数建议

```go
err = ctx.SaveUploadedFile(alertPhoto, photoPath)
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "图片存储失败: " + err.Error()
		Response(ctx, http.StatusInternalServerError, http.StatusInternalServerError, dataResponse, "系统内部错误")
		return
	}
```

这是我将从请求中的二进制图片alertPhtot存到本地photoPath路径的操作，你可以了解gin里面SaveUploadedFile函数的功能，应该会帮助到你

### 5.2实现报警视频上传接口---AlertVideo

这个接口完全就是上面接口的重复了，只是逻辑上稍微不同，上面接口会检验报警信息重复了没有，但是这里的接口会检验报警信息是否存在，因为同一条信息是有报警图片（信息）和报警视频的，而视频一定在图片后，并且只是更新数据库alert表中对应报警信息的path_video字段。这里就是唯一要注意的

```go
err = db.Model(&alert).Where("ca_id = ? AND alert_time = ? AND alert_type = ?", caId, alertTime, alertType).Update("path_video", videoPath).Error
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "数据库更新失败: " + err.Error()
		//如果更新失败，就将本地存储的视频删除
		err = os.Remove(videoPath)
		if err != nil {
			dataResponse["message"] = "数据库更新失败，视频删除失败: " + err.Error()
		}
		Response(ctx, http.StatusInternalServerError, http.StatusInternalServerError, dataResponse, "系统内部错误")
		return
```

这是我更新alert表中pathvideo的操作，可以参考，不要忘记了视频同样是要保存到服务器前端后来会访问的