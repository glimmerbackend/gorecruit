package transaction

// import (
// 	"fmt"
// 	"github.com/gin-gonic/gin"
// 	"gorm.io/gorm"
// 	"net/http"
// 	"server/common"
// 	"time"
// )

/*
getInfo.go 这里是给后端返回所有摄像头的检测时间---startTime和endTime、摄像头的检测类型inferClass、摄像头的编号caId、摄像头caId对应的检测框的信息
当然检测框的信息不存在是正常的，因为用户可能没有划定检测框的范围
这里的url为/backend/camera/getInfo 对应的handler函数为GetInfo
**方法为GET**
*/

/*
GetInfo 这里是给后端返回所有摄像头的检测时间---startTime和endTime、摄像头的检测类型inferClass、摄像头的编号caId、摄像头caId对应的检测框的信息
每个信息都是dataResponse的一个键值对，最后使用Response函数来响应请求
status为0表示成功，为1表示失败，为9表示内部错误。
message返回对应的错误描述。
*/
/*
func GetInfo(ctx *gin.Context) {
	//初始化dataResponse响应部分
	dataResponse := make(gin.H)
	//从数据库中获取所有的摄像头的检测时间---startTime和endTime、摄像头的检测类型inferClass、摄像头的编号caId、摄像头caId对应的检测框的信息
	var cameraInfo []common.Camera
	err := common.DB.Find(&cameraInfo).Error
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "数据库查询失败 " + err.Error()
		Response(ctx, http.StatusInternalServerError, http.StatusInternalServerError, dataResponse, "数据库查询失败")
		log.Printf("数据库查询失败 %v", err.Error())
		return
	}

	cameraSingle := make([]gin.H, 0, 10)
	for _, camera := range cameraInfo {
		//初始化一个cameraResponse响应部分
		cameraResponse := make(gin.H)
		//将camera的信息添加到cameraResponse中
		//对于endTime和startTime，需要转换为字符串,格式为"HH:MM"

		//从数据库中获取对应的box信息
		var boxInfo []common.Box
		err := common.DB.Where("ca_id = ?", camera.ID).Find(&boxInfo).Error //查找所有caId为camera.ID的box信息











		//将cameraResponse添加到cameraSingle中

	}
	//响应请求
	dataResponse["cameraInfo"] = cameraSingle
	dataResponse["status"] = 0
	dataResponse["message"] = "成功"
	Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "成功")
	return
}
*/
