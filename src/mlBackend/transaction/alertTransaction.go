package transaction

import (
	"net/http"
	"os"
	"path/filepath"
	"server/common"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

/*
alertTransaction.go
这个部分是对于报警的操作，用到的数据库表是alert,对应的结构体是Alert,是和机器学习视频处理后端交互的部分
功能上有：
	1、报警信息上传:url为: /backend/alert/info 对应的handler函数为: AlertInfo
	**方法为POST**
	上传报警信息，包括的参数为 图片的.jpg文件、 摄像头的编号也就是caId、报警时间戳（为unix时间戳）、报警类型
	其中图片.jpg是二进制文件，存储到本地，然后将路径存储到alert表中，caId、alertTime（报警时间戳）、报警类型（alertType）都存储到alert表中
	这里因为包含二进制信息，我们采用form-data的格式定义接口，避免二进制转为base64会产生将近30%的额外空间
	2、 报警视频上传: url为: /backend/alert/video 对应的handler函数为: AlertVideo
	**方法为POST**
	上传报警视频，包括的参数为 视频的.avi文件、 摄像头的编号也就是caId、报警时间戳（为unix时间戳）、报警类型
	其中视频.avi是二进制文件，存储到本地，然后将路径存储到alert表中，caId、alertTime（报警时间戳）、报警类型（alertType）都存储到alert表中
	这里因为包含二进制信息，我们采用form-data的格式定义接口，避免二进制转为base64会产生将近30%的额外空间
请求的参数基本一致两个功能：
alertPhoto/Video: 二进制文件
caId: 摄像头的编号
alertTime: 报警时间戳
alertType: 报警类型
我们的响应还是一样的，使用dataResponse来存储响应的数据，然后使用Response函数来响应请求
status为0表示成功，为1表示失败，为9表示内部错误。

message返回对应的错误描述。
*/

/*
AlertInfo 上传报警信息，包括的参数为 图片的.jpg文件、 摄像头的编号也就是caId、报警时间戳（为unix时间戳）、报警类型
*/
func AlertInfo(ctx *gin.Context) {
	//初始化dataResponse响应部分
	dataResponse := make(gin.H)
	//从请求中获取图片.jpg文件、摄像头的编号也就是caId、报警时间戳（为unix时间戳）、报警类型
	alertPhoto, err := ctx.FormFile("alertPhoto") //获取图片.jpg文件
	if err != nil {                               //处理异常
		dataResponse["status"] = 1
		dataResponse["message"] = "图片参数错误: " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	caIdString := ctx.PostForm("caId")           //获取摄像头的编号也就是caId
	alertTimeString := ctx.PostForm("alertTime") //获取报警时间戳（为unix时间戳）
	alertType := ctx.PostForm("alertType")       //获取报警类型
	//确保这四个参数都不为空
	if alertPhoto == nil || caIdString == "" || alertTimeString == "" || alertType == "" {
		dataResponse["status"] = 1
		dataResponse["message"] = "参数有缺失: " + "alertPhoto: " + alertPhoto.Filename + " caId:" + caIdString + " alertTime: " + alertTimeString + " alertType: " + alertType
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	//按照时间戳和报警类型和摄像头编号创建文件路径，路径格式为	workDir, _ := os.Getwd()得到的workDir/src/photoSource/alertType/alertTimeString-caId.jpg

	return
}

/*
AlertVideo 用于接收报警视频
请求方式：POST
请求参数：
alertVideo: 报警视频
caId: 摄像头编号
alertTime: 报警时间戳（为unix时间戳）
alertType: 报警类型
返回参数：
status: 状态码
message: 信息
和上面的AlertPhoto差不多，就不再赘述了，实现方式也是几乎一模一样，只是图片换成了视频
不过有个点需要注意，这里传入的caId、alertTime、alertType不是用来在alert表中创建一个新的行，而是用来在alert表中找到对应的行，然后将这行的path_video字段更新为视频的存储路径
*/
func AlertVideo(ctx *gin.Context) {
	dataResponse := make(gin.H)                   //用于存储返回给后端机器学习的数据
	alertVideo, err := ctx.FormFile("alertVideo") //获取报警视频
	if err != nil {                               //如果获取失败，就返回参数错误
		dataResponse["status"] = 1
		dataResponse["message"] = "视频参数错误: " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	caIdString := ctx.PostForm("caId")           //获取摄像头编号,这里是string类型，后面需要转为int类型
	alertTimeString := ctx.PostForm("alertTime") //获取报警时间戳,这里是string类型，后面需要转为int64类型
	alertType := ctx.PostForm("alertType")       //获取报警类型
	if alertVideo == nil || caIdString == "" || alertTimeString == "" || alertType == "" {
		dataResponse["status"] = 1
		dataResponse["message"] = "参数有缺失: " + "alertVideo: " + alertVideo.Filename + " caId:" + caIdString + " alertTime: " + alertTimeString + " alertType: " + alertType
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	alertTime, err := strconv.ParseInt(alertTimeString, 10, 64)
	if err != nil {
		dataResponse["status"] = 1
		dataResponse["message"] = "时间戳格式错误: " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	caId, err := strconv.Atoi(caIdString)
	if err != nil {
		dataResponse["status"] = 1
		dataResponse["message"] = "摄像头编号格式错误: " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	//格式校验结束后，在存视频之前，看看报警信息是否在alert表中，只有报警信息在表中，才能存储视频
	//寻找数据库中的对应行
	db := common.DB
	var alertFind common.Alert
	err = db.Where("ca_id = ? AND alert_time = ? AND alert_type = ?", caId, alertTime, alertType).First(&alertFind).Error
	if err != nil && err != gorm.ErrRecordNotFound { //如果err != nil && err != gorm.ErrRecordNotFound说明数据库查询出错
		dataResponse["status"] = 9
		dataResponse["message"] = "数据库查询错误: " + err.Error()
		Response(ctx, http.StatusInternalServerError, http.StatusInternalServerError, dataResponse, "系统内部错误")
		return
	} else if err == gorm.ErrRecordNotFound || alertFind.ID == 0 { //如果err == gorm.ErrRecordNotFound || alertFind.ID == 0说明数据库内没有这条数据,那无法进行插入/更新操作，
		dataResponse["status"] = 1
		dataResponse["message"] = "数据库内没有对应的报警信息"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	//说明了存在这个报警信息，那就将视频存储到本地，然后去更新alert表中对应的这一行数据，也就是alertFind找到的数据
	//但是如果报警信息里面还有视频路径，那就说明这里是重复的报警信息，不需要再存储视频了，返回内容重复的响应
	if alertFind.PathVideo != "" {
		dataResponse["status"] = 1
		dataResponse["message"] = "报警信息重复"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	//存储视频到本地
	alertTimeString = time.Unix(alertTime, 0).Local().Format("2006-01-02-15-04-05")
	workDir, _ := os.Getwd()
	//路径格式是：workDir/src/videoSource/alertType/alertTimeString-caIdString.avi
	videoPath := workDir + "/src/videoSource/" + alertType + "/" + alertTimeString + "-" + caIdString + ".avi"
	//使用filepath.Clean()函数将路径中的多余的斜杠去掉,标准化路径
	videoPath = filepath.Clean(videoPath)
	err = ctx.SaveUploadedFile(alertVideo, videoPath)
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "视频存储失败: " + err.Error()
		Response(ctx, http.StatusInternalServerError, http.StatusInternalServerError, dataResponse, "系统内部错误")
		return
	}
	//存储视频成功后，就去更新alert表中对应的这一行数据，也就是alertFind找到的数据
	alert := common.Alert{ //将报警信息存储到Alert结构体中,根据caId，alertTime，alertType，来找到需要插入/更新pathVideo字段的行
		CaId:      caId,
		AlertTime: alertTime,
		AlertType: alertType,
		PathVideo: videoPath,
	}
	err = db.Model(&alert).Where("ca_id = ? AND alert_time = ? AND alert_type = ?", caId, alertTime, alertType).Update("path_video", videoPath).Error
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "数据库更新失败: " + err.Error()
		//如果更新失败，就将本地存储的视频删除
		err = os.Remove(videoPath)
		if err != nil {
			dataResponse["message"] = "数据库更新失败，视频删除失败: " + err.Error()
		}
		Response(ctx, http.StatusInternalServerError, http.StatusInternalServerError, dataResponse, "系统内部错误")
		return
	}
	//更新成功后，返回成功信息
	dataResponse["status"] = 0
	dataResponse["message"] = "成功"
	Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "成功")
	return
}
