package transaction

//这里是定义标准的响应格式
import (
	"github.com/gin-gonic/gin"
)

/*
Response 统一返回格式
不同函数部分定义不同
*/
func Response(ctx *gin.Context, httpStatus int, code int, data gin.H, msg string) {
	ctx.JSON(httpStatus, gin.H{
		"code":    code,
		"data":    data,
		"message": msg,
	})
}
