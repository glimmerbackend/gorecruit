# 题目需求

## task0 前置知识掌握（附在交的markdown里面）

我们的功能就是与用户前端和机器学习后端打交道：对于小萌新，在做这个题目之前，需要了解前置知识，大部分就仅仅了解概念，做到能使用（或者说看代码能看懂是在干嘛）

首先的首先，你要使用一款工具：apifox 这是一个国产的api助手，集成了很多其它工具的功能，他的下载不用多说，他的使用的话基本使用参照b站视频 ---- [apifox基础使用](https://www.bilibili.com/video/BV1ae4y1y7bf/?spm_id_from=333.999.0.0&vd_source=2af7d0ce02dcbfd4607844767c60beda)这是官方的视频，我们也只是用了一小部分功能，后面你会在做题的过程中慢慢涉及到这些功能，完全不用担心这个介绍视频有很多名词比如: 接口，响应，请求参数等等听不懂，因为这些是后面你要学习的内容，也就是我们这个前置task0所要去了解的。

在增加一些可能会用到的关于apifox使用的资料链接

[apifox mock使用](https://apifox.com/help/api-mock/intro-to-mock)

[apifox 动态生成请求值](https://apifox.com/help/environment-and-variables/dynamic-values)

[我们项目的apifox的公开链接（永久有效）](https://apifox.com/apidoc/shared-b08df5b9-3511-4c37-9dee-e5e577d0c908)这个文档就是我们的apifox的文档，我们项目完全是参照这个api定义来的，当然有些示例不是很完整，没有去涵盖所有异常情况的响应，因为异常的处理也是我们考察的一个点

你可以根据上面的这个文档来在apifox app里面创建，当然我们这个文档也是由apifox app里面的项目api发布出来的，

这是邀请链接---[apifox app邀请成员](https://app.apifox.com/invite/project?token=Jd6mDst3DFX7s16TnBGRu)

只是这个邀请链接有效期只能7天，所以有时候可能过期了，不过上面的文档内容其实和这个是一样的，所以都行

//下面的问题需要写到你提交给我的WriteUp(WP)里面，md格式

- 网络的知识
  1. 什么是ip；什么是端口号；什么是url；什么是http，http的状态码大致表示什么意思比如500、200、400等等；什么是ip

- 下面的准备有不少可以结合着apifox上写的内容来理解
  1.   接口是什么？
  2. 请求参数是什么；请求的header是什么，可以包含哪些信息；请求体有哪些格式，大概长什么样（比如json格式、form-data格式等等）；
  3.  响应参数是什么；响应的header是什么，可以包含哪些信息；响应的格式有哪些?(json格式、binary格式)；
  4. 统一的接口是怎么做到前后端开发能够分离的？
  5.   我们项目采用restful风格接口，那么restful风格接口是怎么规范的呢？
  6.   怎么处理异常反馈给前端比较合理(我们项目里面的方式只是一种，不一定是最好的，不过都可以了解怎么反馈错误给前端)


- 关于数据库要有了解：

  1. 什么是数据库；我们为什么需要数据库；数据库的CRUD操作是什么；sql语句是什么；

  2. go里面怎么用数据库的，一般来说我们是使用ORM框架，那么ORM框架大致内容需要知道（就是他是什么个东西），（go里面的的orm框架就是gorm）

  3. 数据库上我们是使用mysql数据库，如果想要尝试mongodb数据库也是可以的，只是我这里给你们的代码里面是使用的mysql，其实使用方式很像，至少在go层面的代码上，但是mysql和mongodb是两种不同类型的数据库，这里可以去了解一下区别

  4. mysql的使用方式，下载完了，看看怎么创建用户，怎么创建数据库，然后稍微试试用sql语句在创建的数据库下创建表。

     比如我就是这样建立一个名称为glimmer的用户的![image-20230802193339796](https://fastly.jsdelivr.net/gh/KMSorSMS/picGallery/img/202308021955635.png)

- unix时间戳的概念

  因为我们用到了许多与时间相关的内容，存在数据库里面的时间可不能是字符串了，因为我们会有不少比较的操作，使用的是unix时间戳,[理解unix时间戳是什么，时区是什么](https://blog.mango.im/posts/timestamp-and-time-zone/#:~:text=UNIX%E6%97%B6%E9%97%B4%E6%88%B3%EF%BC%9A%E6%98%AF%E4%BB%8EUTC%E6%97%B6%E9%97%B41970%E5%B9%B41%E6%9C%881%E6%97%A5%E8%B5%B7%E5%88%B0%E7%8E%B0%E5%9C%A8%E7%9A%84%E7%A7%92%E6%95%B0%EF%BC%8C%E4%B8%8D%E8%80%83%E8%99%91%E9%97%B0%E7%A7%92%EF%BC%8C%E4%B8%80%E5%A4%A9%E6%9C%8986400%E7%A7%92%EF%BC%8C%E5%AE%83%E6%98%AF%E5%92%8C%E6%97%B6%E5%8C%BA%E6%97%A0%E5%85%B3%E7%9A%84%EF%BC%8C%E6%97%A0%E8%AE%BA%E5%9C%A8%E5%9C%B0%E7%90%83%E4%B8%8A%E7%9A%84%E9%82%A3%E4%B8%AA%E8%A7%92%E8%90%BD%EF%BC%8C%E5%90%8C%E4%B8%80%E6%97%B6%E5%88%BB%EF%BC%8CUNIX%E6%97%B6%E9%97%B4%E6%88%B3%E9%83%BD%E6%98%AF%E4%B8%80%E6%A0%B7%E7%9A%84%EF%BC%8C%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%9A%84%E6%9C%AC%E5%9C%B0%E6%97%B6%E9%97%B4%E5%B0%B1%E6%98%AF%E6%A0%B9%E6%8D%AE%20Unix%E6%97%B6%E9%97%B4%E6%88%B3,%2B%20%E6%97%B6%E5%8C%BA%E5%B7%AE%20%E8%BD%AC%E6%8D%A2%E8%80%8C%E6%9D%A5%E7%9A%84%E3%80%82)这是一篇不错的资料

  1. unix时间戳是什么；unix时间戳和我们平时的时间加时区的方式有什么关系；unix时间戳有时区的概念吗？

  2. time.duration是什么？

     除了unix时间戳，我们用同样的原理处理每天的时间，比如"8:00"这样的时间，这里我的实现是使用go的time.duration，用纳秒数来表示一天过去的时间，你需要先了解time.duration是什么，然后在后面开始读代码的时候再理清是怎么用它来表示像"8:00"这样的时间的

- 了解一下SHA256加密方式

  1. 它有什么好处，我们的用户密码为什么选择用它加密来在网上传输？

- 对于前面是在使用git工具的同学，给一些常见bug的解决资料：

  [解决gitignore无法排除已经跟踪文件的问题](https://blog.csdn.net/lengyue1084/article/details/122862377)

  [解决放弃跟踪文件](https://blog.csdn.net/qq_37858386/article/details/105133639)

## task1让代码先跑起来

下载对应的软件包，让代码跑起来呀，你需要完成的任务是:

1. 下载mysql，运行mysql软件，在我们源代码的application.yml中看看端口号和ip是否正确

   ![image-20230802145038868](https://fastly.jsdelivr.net/gh/KMSorSMS/picGallery/img/202308021620567.png)

2. 注意要设置你的运行路径，如果你是使用的ide的话就是在运行那里设置你的output directory(就是可执行文件的产生路径，这个自己在src下创建一个bin文件夹哈)，还有一个working directory就是工作目录，你通过os.Getwd()的到的目录就是他。不过这里你的工作路径应该是在src目录下的，如果有报错可以适当修改代码或者你的路径，主要是这里的配置文件路径需要对应找到，对应代码在**server.go**文件的![image-20230803165106536](https://fastly.jsdelivr.net/gh/KMSorSMS/picGallery/img/202308031651754.png)

   

   ![image-20230802191958360](https://fastly.jsdelivr.net/gh/KMSorSMS/picGallery/img/202308021955636.png)如果你不是使用的ide，可以去网上查找设置切换工作目录的方法，应该是设置gopath环境变量：[这是我找的资料可以参考](https://www.cnblogs.com/zuoyang/p/16349388.html#:~:text=%E5%B7%A5%E4%BD%9C%E7%9B%AE%E5%BD%95%EF%BC%88GOPATH%EF%BC%89%201%201%E3%80%81%E5%B7%A5%E4%BD%9C%E7%9B%AE%E5%BD%95%EF%BC%88GOPATH%EF%BC%89%20GOPATH%20%E6%98%AF%20Go%20%E8%AF%AD%E8%A8%80%E4%B8%AD%E4%BD%BF%E7%94%A8%E7%9A%84%E4%B8%80%E4%B8%AA%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F%EF%BC%8C%E5%AE%83%E4%BD%BF%E7%94%A8%E7%BB%9D%E5%AF%B9%E8%B7%AF%E5%BE%84%E6%8F%90%E4%BE%9B%E9%A1%B9%E7%9B%AE%E7%9A%84%E5%B7%A5%E4%BD%9C%E7%9B%AE%E5%BD%95%E3%80%82%20%E5%B7%A5%E4%BD%9C%E7%9B%AE%E5%BD%95%E6%98%AF%E4%B8%80%E4%B8%AA%E5%B7%A5%E7%A8%8B%E5%BC%80%E5%8F%91%E7%9A%84%E7%9B%B8%E5%AF%B9%E5%8F%82%E8%80%83%E7%9B%AE%E5%BD%95%E3%80%82,5%E3%80%81%E5%9C%A8%E5%A4%9A%E9%A1%B9%E7%9B%AE%E5%B7%A5%E7%A8%8B%E4%B8%AD%E4%BD%BF%E7%94%A8%20GOPATH%20%E5%9C%A8%E5%BE%88%E5%A4%9A%E4%B8%8E%20Go%20%E8%AF%AD%E8%A8%80%E7%9B%B8%E5%85%B3%E7%9A%84%E8%B5%84%E6%96%99%E3%80%81%E6%96%87%E7%AB%A0%E4%B8%AD%E6%8F%8F%E8%BF%B0%E7%9A%84%20GOPTH%20%E9%83%BD%E6%98%AF%E9%80%9A%E8%BF%87%E7%B3%BB%E7%BB%9F%E5%85%A8%E5%B1%80%E7%9A%84%E7%8E%AF%E5%A2%83%E5%8F%98%E9%87%8F%E6%9D%A5%E5%AE%9E%E7%8E%B0%E7%9A%84%E3%80%82%20)

3. 开始运行程序，让它在调试状态运行，能够打断点调试

   数据库的环节可能会遇到Access denied for user 'glimmer'@'%' to database 'monitordb'这种情况，这是因为权限问题，![image-20230802194230363](https://fastly.jsdelivr.net/gh/KMSorSMS/picGallery/img/202308021955638.png)你可以看到我这里复现这个问题的原因，我的glimmer用户的权限为usage，是一个特别低的权限，这里我们直接给他最高权限![image-20230802194445183](https://fastly.jsdelivr.net/gh/KMSorSMS/picGallery/img/202308021955639.png)

3. 使用apifox，试着mock数据，发送你构造的例子，按照文档上说明的请求格式发送数据，看看登录模块是 怎么处理的，看看错误信息是怎么返回的，这个题不要求提交，只是让你能跑起来，感受一下后端是怎么工作的，后面你学习登录模块代码写后续内容也是需要这个模式

这个task你只需要提交能够表示你已经正常在运行题目代码，可以正常测试登录模块，即可，后续的版块就是代码书写环节，你很多地方都可以借鉴着写好的登录模块写，的确在功能上其它模块和他很相似

**后续的task分别在前端文件夹和后端文件夹的README.md中，建议先看前端板块的。**

