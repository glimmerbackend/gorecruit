package main

import (
	"log"
	"server/common"
	transactionFront "server/interactFront/transaction"
	transactionBackend "server/mlBackend/transaction"

	"github.com/gin-gonic/gin"
)

// CollectRoute 所有的路由表
func CollectRoute(r *gin.Engine) *gin.Engine {
	r.POST("/usr", transactionFront.UsrLogin)                                      //登录接口
	r.POST("/usr/register", common.AuthMiddleware(), transactionFront.UsrRegister) //注册接口,需要认证
	r.GET("/usr", common.AuthMiddleware(), transactionFront.UsrGet)                //获取用户信息接口,需要认证
	r.PUT("/usr", common.AuthMiddleware(), transactionFront.UsrInfoChange)         //修改用户信息接口,需要认证;管理员权限
	r.DELETE("/usr", common.AuthMiddleware(), transactionFront.UsrDelete)          //删除用户接口,需要认证;管理员权限
	r.POST("/camera", common.AuthMiddleware(), transactionFront.AddCamera)         //添加摄像头接口,需要认证")
	r.DELETE("/camera", common.AuthMiddleware(), transactionFront.DeleteCamera)    //删除摄像头接口,需要认证")
	r.GET("/camera", common.AuthMiddleware(), transactionFront.GetCamera)          //获取摄像头接口,需要认证")
	r.PUT("/camera", common.AuthMiddleware(), transactionFront.UpdateCamera)       //更新摄像头接口,需要认证")
	r.POST("/box", common.AuthMiddleware(), transactionFront.AddBox)               //添加区域接口,需要认证")
	r.DELETE("/box", common.AuthMiddleware(), transactionFront.DeleteBox)          //删除区域接口,需要认证")
	r.GET("/box", common.AuthMiddleware(), transactionFront.GetBox)                //获取区域接口,需要认证")
	r.GET("/alert", common.AuthMiddleware(), transactionFront.GetAlert)            //获取报警信息接口,需要认证")
	r.DELETE("/alert", common.AuthMiddleware(), transactionFront.DeleteAlert)      //删除报警信息接口,需要认证")
	//r.GET("/alert/src", common.AuthMiddleware(), transactionFront.GetAlertSrc)     //获取报警信息图片/视频接口,需要认证")
	r.GET("/log", common.AuthMiddleware(), transactionFront.GetLog) //获取日志接口,需要认证")

	r.POST("/backend/alert/info", transactionBackend.AlertInfo)   //接收后端的报警信息图片
	r.POST("/backend/alert/video", transactionBackend.AlertVideo) //接收后端的报警视频
	// r.GET("/backend/camera/info", transactionBackend.GetInfo)     //获取后端的摄像头具体信息
	r.GET("/backend/camera/", transactionFront.GetCamera) //获取后端的摄像头区域信息

	r.GET("/ws/alert", common.AuthMiddleware(), transactionFront.WSAlertClient) //获取报警信息接口,需要认证

	log.Printf("router register success")
	return r
}
