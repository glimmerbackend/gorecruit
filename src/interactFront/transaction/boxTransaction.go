package transaction

import (
	"log"
	"net/http"
	"server/common"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

/*
boxTransaction.go
这个部分是对于检测框的操作，用到的数据库表是box,对应的结构体是Box
功能上有：
	1、检测框增加: url为: /box/ 对应的handler函数为: AddBox
	2、检测框删除: url为: /box/ 对应的handler函数为: DeleteBox
	3、检测框获取: url为: /box/ 对应的handler函数为: GetBox
*/

/*
AddBox 函数用于添加检测框：注册的路由url为: /box/
请求参数：

	{
	    "caId": int,
	    "leftUp": "string",
	    "rightDown": "string"
	}

响应部分：
只有status和message两个字段，其中status int:0表示成功，1表示失败 9表示系统内部错误
检测框可以有很多个，每次只是添加一个，所以数据库校验唯一性不重复的时候，是caId以及leftUp和rightDown的组合不能重复
*/
func AddBox(ctx *gin.Context) {
	//初始化dataResponse响应部分
	dataResponse := make(gin.H)
	//得到数据库连接,并且校验数据库连接是否成功
	db := common.DB
	if db == nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "系统内部错误"
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "数据库连接失败")
		return
	}
	//创建一个Box结构体用来接收参数
	var box common.Box
	//绑定参数
	err := ctx.ShouldBindJSON(&box)
	if err != nil {
		dataResponse["status"] = 1
		dataResponse["message"] = "参数错误"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		log.Printf("在boxTransaction.go 中 addBox参数错误: %v", err)
		return
	}
	//校验caId、leftUp、rightDown是否为空，为空的话返回参数格式错误
	if box.CaId == 0 || box.LeftUp == "" || box.RightDown == "" {
		dataResponse["status"] = 1
		dataResponse["message"] = "参数格式错误"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数格式错误")
		return
	}
	//校验caId与leftUP 和 rightDown的组合是否已经在数据库的box表里面，防止重复添加，这里应该是caId和leftUp和rightDown的组合不能重复
	var boxTemp common.Box
	err = db.Where("ca_id = ? AND left_up = ? AND right_down = ?", box.CaId, box.LeftUp, box.RightDown).First(&boxTemp).Error
	//保证数据库里面没有这个数据，也就是说caId和leftUp和rightDown的组合不能重复，err == gorm.ErrRecordNotFound，如果不等于这个，就是数据库里面已经有了这个数据，返回参数错误
	if err == nil || err != gorm.ErrRecordNotFound || boxTemp.CaId != 0 { //校验数据库里面是否已经有了这个数据
		dataResponse["status"] = 1
		dataResponse["message"] = "数据已经存在"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "数据已经存在")
		log.Printf("在boxTransaction.go 中 addBox数据已经存在: %v", err)
		return
	}
	//将数据填入数据库的Box表里面
	err = db.Create(&box).Error
	if err != nil {
		dataResponse["status"] = 1
		dataResponse["message"] = "添加失败"
		Response(ctx, http.StatusInternalServerError, http.StatusInternalServerError, dataResponse, "添加失败")
		log.Printf("在boxTransaction.go 中 addBox添加失败: %v", err)
		return
	}
	//返回成功
	dataResponse["status"] = 0
	dataResponse["message"] = "添加成功"
	Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "添加成功")
	return
}

/*
DeleteBox 函数用于删除检测框：注册的路由url为: /box/
请求参数：
caId int用于指定删除哪个摄像头的检测框
返回参数：
status: int 0表示成功，1表示失败 9表示系统内部错误
message: string 用于返回错误信息
*/

func DeleteBox(ctx *gin.Context) {
	//初始化dataResponse响应部分
	
	//得到数据库连接,并且校验数据库连接是否成功









	//得到参数









	//校验参数是否为空






	//硬删除box.CaId对应的box数据
	
	






	//返回成功
	
	



	
	return
}

/*
GetBox 函数用于获取检测框：注册的路由url为: /box/
请求参数：
caId int用于指定获取哪个摄像头的检测框，这里是返回caId对应的所有检测框,返回格式是"leftUp,rightDown|leftUp,rightDown|leftUp,rightDown"的形式
返回参数：
status: int 0表示成功，1表示失败 9表示系统内部错误
message: string 用于返回错误信息
box: string 用于返回检测框的信息返回的字符串是"leftUp,rightDown|leftUp,rightDown|leftUp,rightDown"的形式
*/
func GetBox(ctx *gin.Context) {
	
	





























































	return
}
