package transaction

import (
	"fmt"
	"log"
	"net/http"
	common2 "server/common"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

/*
	usrTransaction.go
	用户相关的交互
	包括：
		登录	UsrLogin
		注册	UsrRegister
		用户获取 UsrGet 返回当前系统的所有用户信息。
		用户修改 UsrInfoChange 传入想要修改的用户信息，返回修改成功与否。
		用户删除 UsrDelete 输入想要删除的用户名称，返回删除与否的消息。
*/

//// 每次根据不同的接口需求制定合适的data段传入response
//type data struct {
//	Status   int      `json:"status"`
//	Message  string   `json:"message"`
//	UserInfo []string `json:"userInfo,omitempty"` //省略空,是一个字符串切片，使用前最好初始化一个合适大小
//}

/*
UsrLogin 登录接口

	输入：用户名和密码
	输出：登录成功与否

效果详细描述：

 1. 从前端获取用户名和密码：json格式为：
    {
    "username": "liamY",
    "passwd": "123456"
    }

 2. 从数据库中查询是否存在该用户，如果存在，返回登录成功，否则返回登录失败。返回json格式为：

    {
    "code": 200,
    "message": "success",
    "data":
    {
    "message": "success",
    "status": 0,
    "authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJIdWQiOjEsIlJvbGUiOjAsImV4cCI6MTYxMjUwNjQyMCwiaWF0"
    }
    }

这里status的含义为：任务状态  0：登录成功  1：登录失败，但还有机会尝试  2：登录失败超过3次，账户冻结  9：发生错误

3.对于失败用户，要记录失败次数，更新数据库中用户表对应的记录次数，超过三次后有一个冻结操作，冻结时间为一天。后续管理员用户可以修改这个时间
*/
func UsrLogin(ctx *gin.Context) {
	//从前端的body体里面的json中获得用户名字和密码（为SHA256加密的字符串）也就是数据库中存储的内容 ，只需要对比数据库中的内容是否一致即可
	db := common2.DB //获取数据库连接
	//初始化data结构体，status为9，message为空，userInfo为空
	var dataResponse gin.H
	dataResponse = make(gin.H) //初始化map
	//如果db为空，说明数据库连接失败
	if db == nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误"
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "数据库连接失败")
		log.Fatal("database connect failed")
		return
	}
	//获取密码和用户名
	var usr common2.User
	if err := ctx.ShouldBindJSON(&usr); err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "请求格式错误 " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "请求的用户名和密码的名称格式不符合要求")
		log.Printf("在usrTransaction.go中，UsrLogin函数中，获取用户名和密码失败，错误信息为：%s", err.Error())
		return
	}
	//查询数据库中是否存在该用户
	var usrInDB common2.User
	if err := db.Where("name = ?", usr.Name).First(&usrInDB).Error; err != nil {
		dataResponse["status"] = 1
		dataResponse["message"] = "用户不存在"
		Response(ctx, http.StatusNotFound, http.StatusNotFound, dataResponse, "该用户不存在")
		log.Printf("在usrTransaction.go中，UsrLogin函数中，查询数据库中是否存在该用户失败，错误信息为：%s", err.Error())
		return
	}

	//先判定是否冻结，以及是否在有效期,即deadlineTime如果小于当前时间，说明已经过期，releaseTime如果大于当前时间，说明已经冻结
	//但是注意，我们的deadlineTime和releaseTime是用的unix时间戳形式，也就是他的大小是以秒为单位的，
	if usrInDB.DeadlineTime < time.Now().Unix() || usrInDB.ReleaseTime > time.Now().Unix() {
		dataResponse["status"] = 2
		dataResponse["message"] = "用户已冻结/过期"
		Response(ctx, http.StatusForbidden, http.StatusForbidden, dataResponse, "该用户已冻结/过期")
		return
	}
	//如果存在该用户，判断密码是否正确
	if usrInDB.Passwd == usr.Passwd {
		//判断处于loginStart和loginEnd之间，思路是得到当前时间，从当前时间中分离出时分秒时间戳，将它们加和与start和end的两个时间戳比大小
		hour, min, sec := time.Duration(time.Now().Hour()), time.Duration(time.Now().Minute()), time.Duration(time.Now().Second()) //获取当前时间的时分秒
		//将时分秒转换为时间戳
		nowTime := hour*time.Hour + min*time.Minute + sec*time.Second
		//比较nowTime是否在loginStart和loginEnd之间
		//而nowTime、loginStart和loginEnd是以纳秒为单位的，这是场景不同
		if int64(nowTime) > usrInDB.LoginStart && int64(nowTime) < usrInDB.LoginEnd { //如果在登录时间段内
			dataResponse["status"] = 0
			dataResponse["message"] = "登录成功"
			//生成jwt token
			token, err := common2.GetToken(usrInDB)
			if err != nil {
				dataResponse["status"] = 9
				dataResponse["message"] = "server内部错误 " + err.Error()
				Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "生成token失败")
				log.Fatal("generate token failed")
				return
			}
			dataResponse["authorization"] = token
			Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "登录成功")
			return
		} else { //如果不在登录时间段内
			dataResponse["status"] = 1
			dataResponse["message"] = "不在登录时间段内"
			Response(ctx, http.StatusForbidden, http.StatusForbidden, dataResponse, "不在登录时间段内(冻结、失效、不在时间段)")
			return
		}
	}
	//如果密码不正确，失败次数的登记加一
	dataResponse["status"] = 1 //登录失败，但还有机会尝试
	usrInDB.FailTimes++
	dataResponse["message"] = "登录失败，但还有" + strconv.Itoa(3-usrInDB.FailTimes) + "次机会"
	if usrInDB.FailTimes >= 3 { //如果失败次数超过3次，冻结账户
		usrInDB.FailTimes = 0 //失败次数清零
		//冻结账户,将ReleaseTime设置为一天以后的时间，unix时间戳表示
		usrInDB.ReleaseTime = time.Now().Add(24 * time.Hour).Unix() //当前时间加上一天的时间再转换为unix时间戳
		dataResponse["status"] = 2                                  //冻结账户
		dataResponse["message"] = "登录失败超过3次，账户冻结"
	}
	//更新数据库中的数据
	if err := db.Save(&usrInDB).Error; err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "数据库更新出错: " + err.Error()
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "数据库更新失败")
		log.Printf("在usrTransaction.go中，UsrLogin函数中，更新数据库中的数据失败，错误信息为：%s", err.Error())
		return
	}
	Response(ctx, http.StatusForbidden, http.StatusForbidden, dataResponse, "登录失败")
	return
}

/*
UsrRegister 注册接口，这个接口只能是管理员用户使用
关于请求参数介绍：
| 参数名           | 数据类型 | 说明                                                   |
| ---------------- | -------- | ------------------------------------------------------ |
| username         | string   | 用户名字段用来查找密码以及冻结期等字段                 |
| passwd            | string   | 密码字段采用SHA256加密的字符串，用于匹配用户名         |
| role             | int      | 用户角色，用于区分用户权限，1为管理员用户，2为普通用户 |
| deadlineTime    | string   | 有效期字段，用于限制用户的使用时长                     |
| loginStart | string   | 开始登录时间，该字段用于限定该用户每日开始登录的时间   |
| loginEnd   | string   | 结束登录时间，该字段用于限定该用户每日结束登录的时间   |
对于返回参数
| 参数名  | 数据类型 | 说明                                                         |
| ------- | -------- | ------------------------------------------------------------ |
| status  | int      | 任务状态  0：注册成功  1：注册失败，详细原因在message字段描述  9：发生错误，详细错误在message字段描述 |
| message | String   | 状态描述                                                     |
*/
func UsrRegister(ctx *gin.Context) {
	//先从middleWare处理后的context中拿到用户数据
	//初始化data结构体，status为9，message为空，userInfo为空
	var dataResponse gin.H
	dataResponse = make(gin.H) //初始化map
	usrInterface, ok := ctx.Get("user")
	if ok != true {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误"
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "从context中获取用户信息失败")
		log.Fatal("get user from context failed")
		return
	}
	//将usr从interface{}转换为common.User类型
	usr, ok := usrInterface.(common2.User)
	if ok != true {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误"
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "从interface{}转换为common.User类型失败")
		log.Fatal("convert interface{} to common.User failed")
		return
	}
	//校验用户权限，只有管理员用户才能注册用户
	if usr.Role != 1 {
		dataResponse["status"] = 1
		dataResponse["message"] = "用户权限不足"
		Response(ctx, http.StatusForbidden, http.StatusForbidden, dataResponse, "用户权限不足，无法注册用户")
		return
	}
	//获取数据库连接
	db := common2.DB
	if db == nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误"
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "数据库连接失败在UsrRegister")
		log.Fatal("database connect failed in UsrRegister")
		return
	}
	//获取请求参数
	//这里需要从请求参数中拿到deadlineTime参数的值，它是一个字符串，比如"2023-15-10"，没有时分秒，需要把它转化为unix时间戳
	json := make(gin.H)
	err := ctx.BindJSON(&json)
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "请求格式错误 " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "你的请求参数名称不符合规范")
		return
	}
	//将用户信息放入User结构体中，usrInDB要初始化
	var usrInDB common2.User
	var okSingle bool
	usrInDB.Name, okSingle = json["name"].(string)
	ok = ok && okSingle //如果ok为false，说明有一个转换失败
	usrInDB.Passwd, okSingle = json["passwd"].(string)
	ok = ok && okSingle
	if _, okSingle = json["role"].(float64); okSingle == true { //如果role字段存在,这里是因为我们的role想要用int类型，但是在json中是float64类型，所以需要转换
		usrInDB.Role = int(json["role"].(float64))
	}
	ok = ok && okSingle
	deadlineTimeString, okSingle := json["deadlineTime"].(string)
	ok = ok && okSingle
	//同样对于loginStart参数和loginEnd参数，也需要转换为unix时间戳,他们的格式是 "8:00" "20:00"这种
	loginStartTimeString, okSingle := json["loginStart"].(string)
	ok = ok && okSingle
	loginEndTimeString, okSingle := json["loginEnd"].(string)
	ok = ok && okSingle
	//校验是否所有参数都齐了
	if ok != true { //如果有一个转换失败，说明请求参数不符合规范
		dataResponse["status"] = 9
		dataResponse["message"] = "请求格式错误"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "你的请求参数名称不符合规范")
		return
	}
	deadlineTime, err := time.ParseInLocation("2006-01-02", deadlineTimeString, time.Local)
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "请求格式错误 " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "日期格式错误: "+err.Error())
		log.Printf("在usrTransaction.go中的UsrRegister函数中，日期格式错误: %s", err.Error())
		return
	}
	usrInDB.DeadlineTime = deadlineTime.Unix() //将时间转为unix时间戳
	loginStartTime, err := time.ParseInLocation("15:04", loginStartTimeString, time.Local)
	if err != nil {
		dataResponse["status"] = 1 //如果有一个转换失败，说明请求参数不符合规范
		dataResponse["message"] = "请求格式错误"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "日期格式错误"+err.Error())
		log.Printf("在usrTransaction.go中的UsrRegister函数中，日期格式错误: %s", err.Error())
		return
	}
	loginEndTime, err := time.ParseInLocation("15:04", loginEndTimeString, time.Local)
	if err != nil {
		dataResponse["status"] = 1 //如果有一个转换失败，说明请求参数不符合规范
		dataResponse["message"] = "请求格式错误"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "日期格式错误"+err.Error())
		log.Printf("在usrTransaction.go中的UsrRegister函数中，日期格式错误: %s", err.Error())
		return
	}
	usrInDB.LoginStart = int64(time.Duration(loginStartTime.Hour())*time.Hour + time.Duration(loginStartTime.Minute())*time.Minute)
	usrInDB.LoginEnd = int64(time.Duration(loginEndTime.Hour())*time.Hour + time.Duration(loginEndTime.Minute())*time.Minute)
	//判断usrInDB.Name用户名的用户是否已经在数据库中
	var usrInDBInDB common2.User
	db.Where("name = ?", usrInDB.Name).First(&usrInDBInDB)
	if usrInDBInDB.Name != "" {
		dataResponse["status"] = 1
		dataResponse["message"] = "用户名已存在"
		Response(ctx, http.StatusForbidden, http.StatusForbidden, dataResponse, "用户名已存在")
		return
	}
	//将usrInDB写入数据库,要处理异常
	err = db.Create(&usrInDB).Error
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误 " + err.Error()
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "写入数据库失败")
		log.Fatal("write usrInDB to database failed")
		return
	}
	//返回注册成功
	dataResponse["status"] = 0
	dataResponse["message"] = "注册成功"
	Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "注册成功")
	return
}

/*
	UsrGet 获取用户信息,返回当前系统的所有用户信息
	没有请求体
	返回值：
		对于dataResponse部分，是有status int，message string，userInfo []string 三个部分
		userInfo是一个字符串数组，每个字符串是一个用户的信息，格式为 "name,role,loginStart(为时分格式),loginEnd(为时分格式),deadlineTime(从Unix转为年月日格式),releaseTime(从Unix时间戳转为年月日格式)"
*/

func UsrGet(ctx *gin.Context) {
	//初始化dataResponse
	dataResponse := make(gin.H)
	//访问数据库
	db := common2.DB
	if db == nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误"
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "数据库连接失败在UsrGet")
		log.Fatal("database connect failed in UsrGet")
		return
	}
	//从数据库中获取所有用户信息
	var usrInDBs []common2.User
	//包括异常处理
	err := db.Find(&usrInDBs).Error
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误 " + err.Error()
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "从数据库中获取用户信息失败")
		log.Fatal("get usrInDBs from database failed in UsrGet")
		return
	}
	//将用户信息转化为字符串数组
	var usrInfos []string
	for _, usrInDB := range usrInDBs {
		//把usrInDB的usrInDB.role转换为字符串,1为管理员，2为普通用户
		var role string
		if usrInDB.Role == 1 {
			role = "管理员"
		} else if usrInDB.Role == 2 {
			role = "普通用户"
		}
		//把usrInDB.loginStart的时间（表示为时间长度的duration，比如8:00被表示为一个8*time.Hour的时间长度量），将这种表示方法转化为时分格式的字符串
		loginStart := fmt.Sprintf("%02d:%02d", int(time.Duration(usrInDB.LoginStart).Hours()), int(time.Duration(usrInDB.LoginStart).Minutes())%60) //loginStart的格式是"8:00"这种
		loginEnd := fmt.Sprintf("%02d:%02d", int(time.Duration(usrInDB.LoginEnd).Hours()), int(time.Duration(usrInDB.LoginEnd).Minutes())%60)       //loginEnd的格式是"20:00"这种
		usrInfo := usrInDB.Name + "," + role + "," + loginStart + "," + loginEnd + "," + time.Unix(usrInDB.DeadlineTime, 0).Format("2006-01-02") + "," + time.Unix(usrInDB.ReleaseTime, 0).Format("2006-01-02 15:04:05")
		usrInfos = append(usrInfos, usrInfo)
	}
	//返回用户信息
	dataResponse["status"] = 0
	dataResponse["message"] = "获取用户信息成功"
	dataResponse["userInfo"] = usrInfos
	Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "获取用户信息成功")
	return
}

/*
UsrInfoChange 修改用户信息
这个功能只能由管理员使用
请求体：

	{
	    "name": "string",
	    "passwd": "string",
	    "role": 0,
	    "deadlineTime": "string",
	    "loginStart": "string",
	    "loginEnd": "string",
	    "releaseTime": "string"
	}

对应go的模型例子：
// ApifoxModel

	type ApifoxModel struct {
		DeadlineTime string `json:"deadlineTime"`// 有效期字段，用于限制用户的使用时长，为空则不修改
		LoginEnd     string `json:"loginEnd"`    // 结束登录时间，该字段用于限定该用户每日结束登录的时间，为空则不修改
		LoginStart   string `json:"loginStart"`  // 开始登录时间，该字段用于限定该用户每日开始登录的时间，为空则不修改
		Name         string `json:"name"`        // 用户名字段用来查找密码以及冻结期等字段
		Passwd        string `json:"passwd"`       // 密码字段采用SHA256加密的字符串，用于匹配用户名，为空则不修改
		ReleaseTime  string `json:"releaseTime"` // 冻结字段，在此时间之前，该用户一直处于冻结状态，为空则不修改
		Role         int64  `json:"role"`        // 用户角色，用于区分用户权限，1为管理员用户，2为普通用户，为空则不修改
	}

返回值：
在data字段中，有status int，message string两个字段
status为0表示修改成功，为1表示修改失败，9表示服务器内部错误
message为对status的解释
*/
func UsrInfoChange(ctx *gin.Context) {
	//先从ctx中得到身份信息，校验身份是否是管理员
	reqUsrInterface, ok := ctx.Get("user")
	if !ok {
		log.Fatal("get user from ctx failed in UsrInfoChange")
		return
	}
	//初始化dataResponse
	dataResponse := make(gin.H)
	//从ctx中得到身份信息，校验身份是否是管理员
	reqUsr := reqUsrInterface.(common2.User)
	if reqUsr.Role != 1 {
		dataResponse["status"] = 1
		dataResponse["message"] = "身份信息错误"
		Response(ctx, http.StatusUnauthorized, http.StatusUnauthorized, dataResponse, "身份信息错误，不是管理员")
		return
	}
	//从请求体中获取请求参数
	var usrInDB common2.User
	//这里需要从请求参数中拿到deadlineTime参数的值，它是一个字符串，比如"2023-15-10"，没有时分秒，需要把它转化为unix时间戳
	//还有loginStart和loginEnd，它们是一个字符串，比如"8:00"，需要把它转化为一个time.Duration类型的量，表示为一个时间长度，比如8*time.Hour
	//还有releaseTime，它是一个字符串，比如"2023-15-10 8:00:00"，需要把它转化为unix时间戳
	json := make(gin.H)
	err := ctx.BindJSON(&json)
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "请求格式错误 " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "你的请求参数名称不符合规范")
		log.Printf("在usrTransaction.go中的UsrInfoChange函数中，请求格式错误 %v", err)
		return
	}
	//从json中获取请求参数,只将非空值加入
	if Name, _ := json["name"].(string); Name != "" {
		usrInDB.Name = Name
	}
	if Passwd, _ := json["passwd"].(string); Passwd != "" {
		usrInDB.Passwd = Passwd
	}
	if _, okSingle := json["role"].(float64); okSingle == true { //如果role字段存在,这里是因为我们的role想要用int类型，但是在json中是float64类型，所以需要转换
		usrInDB.Role = int(json["role"].(float64))
	}
	deadlineTimeString, _ := json["deadlineTime"].(string)
	//同样对于loginStart参数和loginEnd参数，也需要转换为unix时间戳,他们的格式是 "8:00" "20:00"这种
	loginStartTimeString, _ := json["loginStart"].(string)
	loginEndTimeString, _ := json["loginEnd"].(string)
	releaseTimeString, _ := json["releaseTime"].(string)
	//将deadlineTimeString转化为unix时间戳,前提是检测到的deadlineTimeString不为空否则不转换
	if deadlineTimeString != "" {
		deadlineTime, err := time.Parse("2006-01-02", deadlineTimeString) //deadlineTimeString只有年月日，没有时分秒
		if err != nil {
			dataResponse["status"] = 9
			dataResponse["message"] = "请求格式错误: " + err.Error()
			Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "你的请求参数名称不符合规范")
			log.Printf("在usrTransaction.go中的UsrInfoChange函数中，请求格式错误 %v", err)
			return
		}
		usrInDB.DeadlineTime = deadlineTime.Unix()
	}
	//将loginStartTimeString转化为time.Duration类型的量,前提是检测到的loginStartTimeString不为空否则不转换
	if loginStartTimeString != "" {
		loginStartTime, err := time.Parse("15:04", loginStartTimeString) //loginStartTimeString只有时分，没有年月日
		if err != nil {
			dataResponse["status"] = 9
			dataResponse["message"] = "请求格式错误: " + err.Error()
			Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "你的请求参数名称不符合规范")
			log.Printf("在usrTransaction.go中的UsrInfoChange函数中，请求格式错误 %v", err)
			return
		}
		usrInDB.LoginStart = int64(time.Duration(loginStartTime.Hour())*time.Hour + time.Duration(loginStartTime.Minute())*time.Minute) //将loginStartTime转化为一个time.Duration类型的量，表示为一个时间长度，比如8*time.Hour
	}
	//将loginEndTimeString转化为time.Duration类型的量,前提是检测到的loginEndTimeString不为空否则不转换
	if loginEndTimeString != "" {
		loginEndTime, err := time.Parse("15:04", loginEndTimeString) //loginEndTimeString只有时分，没有年月日
		if err != nil {
			dataResponse["status"] = 9
			dataResponse["message"] = "请求格式错误: " + err.Error()
			Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "你的请求参数名称不符合规范")
			log.Printf("在usrTransaction.go中的UsrInfoChange函数中，请求格式错误 %v", err)
			return
		}
		usrInDB.LoginEnd = int64(time.Duration(loginEndTime.Hour())*time.Hour + time.Duration(loginEndTime.Minute())*time.Minute) //将loginEndTime转化为一个time.Duration类型的量，表示为一个时间长度，比如8*time.Hour
	}
	//将releaseTimeString转化为unix时间戳,前提是检测到的releaseTimeString不为空否则不转换
	if releaseTimeString != "" {
		releaseTime, err := time.Parse("2006-01-02 15:04:05", releaseTimeString) //releaseTimeString有年月日时分秒
		if err != nil {
			dataResponse["status"] = 9
			dataResponse["message"] = "请求格式错误: " + err.Error()
			Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "你的请求参数名称不符合规范")
			log.Printf("在usrTransaction.go中的UsrInfoChange函数中，请求格式错误 %v", err)
			return
		}
		usrInDB.ReleaseTime = releaseTime.Unix()
	}
	//访问数据库
	db := common2.DB
	if db == nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误"
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "数据库连接失败在UsrInfoChange")
		log.Fatal("database connect failed in UsrInfoChange")
		return
	}
	//从数据库中获取usrInDBInDB
	var usrInDBInDB common2.User
	err = db.Where("name = ?", usrInDB.Name).First(&usrInDBInDB).Error
	if err != nil {
		dataResponse["status"] = 1
		dataResponse["message"] = "用户名不存在"
		Response(ctx, http.StatusForbidden, http.StatusForbidden, dataResponse, "用户名不存在")
		log.Printf("在usrTransaction.go中的UsrInfoChange函数中，用户名不存在 %v", err)
		return
	}
	//将usrInDB写入数据库,要处理异常
	err = db.Model(&usrInDBInDB).Updates(&usrInDB).Error //将usrInDB中非0字段值写入usrInDBInDB中
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "数据库错误: " + err.Error()
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "写入数据库失败")
		log.Fatal("在usrTransaction.go中的UsrInfoChange函数中 write usrInDB to database failed")
		return
	}
	//返回修改成功
	dataResponse["status"] = 0
	dataResponse["message"] = "修改成功"
	Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "修改成功")
	return
}

/*
UsrDelete	删除用户
该功能需要管理员权限
请注意，使用 DELETE 方法删除资源时需要谨慎，因为一旦删除后就无法恢复。
另外，删除资源时应该遵循安全和幂等的原则，即相同的请求多次执行应该产生相同的结果，并且不会对系统状态产生影响。
| 请求参数 |          |                                                              |
| -------- | -------- | ------------------------------------------------------------ |
| 参数名   | 数据类型 | 说明                                                         |
| username | []string    | 用户名可以唯一确定用户，删除数据库数据  ，是一个名称的字符串数组 |
*/
func UsrDelete(ctx *gin.Context) {
	dataResponse := make(gin.H) //用于返回的数据
	//先从ctx中得到身份信息，校验是否为管理员用户
	reqUserInterface, _ := ctx.Get("user")
	reqUser := reqUserInterface.(common2.User)
	if reqUser.Role != 1 {
		dataResponse["status"] = 1
		dataResponse["message"] = "权限不足"
		Response(ctx, http.StatusForbidden, http.StatusForbidden, dataResponse, "权限不足")
		return
	}
	//从ctx中得到请求参数,通过先解析到json(gin.H)的map里面，再从map里面取出来
	json := make(gin.H)
	err := ctx.BindJSON(&json)
	if err != nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "请求格式错误 " + err.Error()
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "请求格式错误")
		log.Printf("在usrTransaction.go中的UsrDelete函数中，请求格式错误 %v", err)
		return
	}
	//从json中取出username
	username, ok := json["username"].([]interface{}) //username是一个字符串数组
	if !ok {
		dataResponse["status"] = 9
		dataResponse["message"] = "请求格式错误"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "请求格式错误")
		return
	}
	//将username转化为字符串数组
	usernameString := make([]string, len(username))
	for i, v := range username {
		usernameString[i] = v.(string)
	}
	//访问数据库
	db := common2.DB
	if db == nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "server内部错误"
		Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "数据库连接失败在UsrDelete")
		log.Fatal("database connect failed in UsrDelete")
		return
	}
	//从数据库中删除用户
	for _, v := range usernameString {
		//这里的删除方式是硬删除，即直接从数据库中删除，不是软删除
		err = db.Unscoped().Where("name = ?", v).Delete(&common2.User{}).Error //使用Unscoped()方法可以删除包括软删除的数据
		if err != nil {
			dataResponse["status"] = 9
			dataResponse["message"] = "删除用户失败: " + v + " 请检查用户名是否存在"
			Response(ctx, http.StatusServiceUnavailable, http.StatusServiceUnavailable, dataResponse, "server内部错误 "+err.Error())
			log.Fatal("在usrTransaction.go中的UsrDelete函数中 delete user failed " + err.Error())
			return
		}
	}
	//返回删除成功
	dataResponse["status"] = 0
	dataResponse["message"] = "删除成功"
	Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "删除成功")
	return
}
