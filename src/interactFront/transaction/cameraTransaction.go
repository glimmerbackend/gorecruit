package transaction

import (
	"github.com/gin-gonic/gin"
)

/*
cameraTransaction.go
这个部分是与摄像头交互的部分，数据库用到的表是Camera结构体所对应的表
功能上有：
	1、摄像头增加: url为: /camera/add 对应的handler函数为: AddCamera
		传入新增摄像头的必要参数，返回是否添加成功
	2、摄像头删除: url为: /camera/delete 对应的handler函数为: DeleteCamera
		传入想要删除的摄像头名称列表，返回是否删除成功
	3、摄像头获取: url为: /camera/get 对应的handler函数为: GetCamera
		传入想要获取的摄像头名称列表或者“all”，返回摄像头的url列
	4、摄像头(时间)修改: url为: /camera/update 对应的handler函数为: UpdateCamera
		传入想要修改的摄像头名称，并且传入startTime和endTime，返回是否修改成功
*/

/*
AddCamera 函数用于添加摄像头：注册的路由url为: /camera/
| 请求参数   |          |                                  |
| ---------- | -------- | -------------------------------- |
| 参数名     | 数据类型 | 说明                             |
| name       | string   | 为摄像头起的名称                 |
| ip         | string   | 该字段为访问摄像头的网络ip       |
| port       | string   | 该字段为访问摄像头的网络port     |
| user       | string   | 该字段为登录该摄像头的账号字段   |
| passwd     | string   | 该字段为登录该摄像头的密码字段   |
| channel    | int      | 该字段为访问摄像头中画面的字段   |
| area       | string   | 区域，限定该摄像头来自哪个辖区   |
| startTime  | string   | 该摄像头开始检测时间             |
| endTime    | string   | 该摄像头结束检测时间             |
| inferClass | []string/inferClass(type inferClass []string) | 该字段为描述该摄像头检测类型字段 |
对于响应的dataResponse部分：
status int ： 0表示成功，1表示失败 9表示未知错误
message string ： 返回的信息
*/
func AddCamera(ctx *gin.Context) {
	//初始化dataResponse响应部分
	
	//得到数据库连接,并且校验数据库连接是否成功
	
	//创建一个Camera结构体用来接收参数
	
	//绑定参数,这里对于参数的startTime和endTime的绑定需要特殊处理,因为他们传入的是如"8:00"这样的字符串,需要转为一个duration也就是time.Duration类型对应的int64大小的值
	//所以我们这里还是用一个gin.H类型的json变量来接收数据,将数据处理过后再转换为Camera结构体
	



	//将json中的数据转换为Camera结构体,这里面参数都是不可空缺的，使用singleOk记录单次的转换是否成功，用ok记录所有的转换是否成功
	
	













	//这里的startTime和endTime需要特殊处理,先取出他们字符串形式
	
	






	//先判定是否参数齐了
	




	//camera的数据库里面name是唯一的，所以我们需要先判断该摄像头是否已经存在
	//包括异常处理

	





	//再将他们转换为time.Duration类型
	






	
	//放入camera结构体中


	//处理inferClass字段,在json中是一个[]string类型，强制转换为inferClass类型
	
	


	//将数据插入数据库





	//返回成功信息


	return

}

/*
DeleteCamera : 删除摄像头
method : DELETE
url : /camera/
在请求体里面只有一个参数name，表示要删除的摄像头的名字
responseData有status和message两个字段
status为0表示成功，为1表示失败 9表示系统内部错误
message为提示信息
请注意，使用 DELETE 方法删除资源时需要谨慎，因为一旦删除后就无法恢复。
另外，删除资源时应该遵循安全和幂等的原则，即相同的请求多次执行应该产生相同的结果，并且不会对系统状态产生影响。
*/
func DeleteCamera(ctx *gin.Context) {
	//初始化返回数据
	
	//获取参数
	
	




	//判定参数是否为空






	//连接数据库
	
	




	//删除数据
	
	



	//返回成功信息





	return
}

/*
GetCamera : 获取摄像头信息
传入想要获取的摄像头名称列表或者“all”，返回摄像头的url列,all代表返回所有摄像头的url,如果没有摄像头名称列表或者为空就相当于all
请求的names是一个[]string类型
method : GET
url : /camera/
在请求体里面只有一个参数name，表示要获取的摄像头的名字列表
responseData有status、message还有urls字段，urls是一个[]string类型，表示摄像头的url列表
*/
func GetCamera(ctx *gin.Context) {
	//初始化返回数据


	//获取参数,参数里面只有一个name数组（也有可能不存在），用一个结构体来接收（尝试不同接收方式）




	
	//连接数据库





	//查询数据
	
	

	//判定参数是否为空,如果为空则返回所有摄像头的url，也就是与names[0] == "all"相同
	
	



	//返回成功信息
	
	
	//创建一个map数组,长度为cameras的长度
	
	
	//设定dataResponse["url"]对应的是一个数组,这个数组里面的每个元素是个map（gin.H），每个map都是一个摄像头的全部信息，包括ip,port,user,passwd,channel,name,id(也就是box里面对应的caId),开始时间,结束时间
	//格式为：["ip"] = ip,["port"] = port,["user"] = user,["passwd"] = passwd,["channel"] = channel,["name"] = name,["id"] = id,["startTime"] = startTime,["endTime"] = endTime
	
	



















	
	return
}

/*
UpdateCamera : 更新摄像头信息
传入想要更新的摄像头信息
method : PUT
url : /camera/
在请求体里面有一个参数name,是要更新的摄像头的名字,有startTime和endTime两个参数,分别是开始时间和结束时间,他们格式为"8:00"这种形式，需要在后端转换为time.duration类型
responseData有status、message字段,和前面的规范一样
status = 0表示更新成功 1表示更新失败 9表示系统内部错误
message表示更新成功或者失败的信息
*/
func UpdateCamera(ctx *gin.Context) {
	//初始化返回数据
	
	
	//获取参数



	
	 //如果参数不完整
	
	


	
	//将时间转换为time.duration类型 并创建需要更新的camera结构体对象
	
	




















	//连接数据库
	
	







	//更新数据,如果没有这个摄像头需要向用户返回错误信息
	//先查询是否有这个摄像头















	//返回成功信息
	
	




	return
}
