package transaction

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

/*
log.go 返回日志信息
日志获取 url为: /log 方法为GET 对应的handler函数为: GetLog
*/

/*
GetLog 返回日志信息
日志获取 url为: /log 方法为GET 对应的handler函数为: GetLog
请求参数: date: 日期 字符串(string) 格式为: 2020-12-12
返回对应天的日志信息
返回一个文件，这个文件是对应天的日志信息
日志文件的路径为: workDir, _ := os.Getwd() 得到的workDir/src/log/2020-12-12-server.log
*/
func GetLog(ctx *gin.Context) {
	//从请求中获取日期，请求是json格式的，所以用BindJSON
	var date struct {
		Date string `json:"date"`
	}
	err := ctx.BindJSON(&date)
	if err != nil {
		_ = ctx.AbortWithError(http.StatusBadRequest, err)
		log.Printf("GetLog: %v", err)
		return
	}
	//获取日志文件的路径、路径为: workDir, _ := os.Getwd() 得到的workDir/src/log/(2020-12-12)(日期)-server.log
	workDir, err := os.Getwd()
	if err != nil {
		err = ctx.AbortWithError(http.StatusInternalServerError, err)
		if err != nil {
			log.Printf("GetLog: %v", err)
		}
		return
	}
	//这里要保证data.Date的格式为: 2020-12-12,可能用户的输入是2022-1-3，我们用时间格式化的方式来保证格式
	t, err := time.Parse("2006-1-2", date.Date)
	if err != nil {
		err = ctx.AbortWithError(http.StatusBadRequest, err)
		if err != nil {
			log.Printf("GetLog: %v", err)
		}
		return
	}
	//得到格式化后的日期
	date.Date = t.Format("2006-01-02")
	logPath := workDir + "/src/log/" + date.Date + "-server.log"
	//使用filepath.Clean()函数清理路径
	logPath = filepath.Clean(logPath)
	//判断文件是否存在
	_, err = os.Stat(logPath)
	if err != nil {
		err = ctx.AbortWithError(http.StatusBadRequest, err)
		if err != nil {
			log.Printf("GetLog: %v", err)
		}
		return
	}
	//打开日志文件
	_, err = os.Open(logPath)
	if err != nil {
		err = ctx.AbortWithError(http.StatusInternalServerError, err)
		if err != nil {
			log.Printf("GetLog: %v", err)
		}
		return
	}
	//返回日志文件
	//设置响应头,包含文件的大小信息
	//得到文件大小
	fileInfo, err := os.Stat(logPath)
	if err != nil {
		err = ctx.AbortWithError(http.StatusInternalServerError, err)
		if err != nil {
			log.Printf("GetLog: %v", err)
		}
		return
	}
	//设置响应头
	ctx.Header("Content-Type", "application/octet-stream")
	ctx.Header("Content-Length", strconv.FormatInt(fileInfo.Size(), 10))
	ctx.Header("Content-Disposition", "attachment; filename="+date.Date+"-server.log")
	ctx.FileAttachment(logPath, date.Date+"-server.log")
	return
}
