package transaction

import (
	"net/http"
	"os"
	"path/filepath"
	"server/common"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

/*
AlertTransaction.go
报警信息交互，提供的功能是：
1. 报警信息获取	GET		/alert 	GetAlert
2. 报警信息删除	DELETE	/alert  DeleteAlert
3. 报警资源(视频or图片)获取	GET		/alert/src  GetAlertSrc
*/

/*
GetAlert 函数用于获取报警信息：注册的路由url为: /alert
| 请求参数   |          |                                            |
| ---------- | -------- | ------------------------------------------ |
| 参数名     | 数据类型 | 说明                                       |
| startDate  | string   | 需要搜索的开始日期，全部搜索输入“”         |
| endDate    | string   | 需要搜索的结束日期，全部搜索输入“”         |
| cameraName | list     | 需要搜索的摄像头名称列表，全部搜索输入[“”] |
| type       | list     | 需要搜索的检测类型列表，全部搜索输入[“”]   |
返回参数：
dataResponse 里面除了标准的status和message外，使用infos字段返回报警信息列表，infos里面的每一个元素是一个map，包含了报警信息的所有字段,这些字段来源于Alert结构体
有["caId"]:是alert.CaId，["type"]:是alert.Type，["alertTime"]:是alert.AlertTime，["videoPath"]:是alert.PathVideo，["photoPath"]:是alert.PathPhoto
status 0:成功 1:请求失败 9.系统错误
*/
func GetAlert(ctx *gin.Context) {
	return
}

/*
DeleteAlert 删除报警信息、
参数：pathVideos、pathPhotos
pathVideos：视频路径列表,也就是既要删除数据库alert表里面的记录，也要删除视频文件
pathPhotos：图片路径列表，也就是既要删除数据库alert表里面的记录，也要删除图片文件
返回：status、message
status：0表示成功，1表示失败，9表示系统内部错误
message：返回的信息
*/
func DeleteAlert(ctx *gin.Context) {
	//初始化响应的dataResponse
	dataResponse := make(gin.H)
	//获取参数,这里的参数是一个json，里面有两个数组，一个是视频路径数组，一个是图片路径数组
	var params struct {
		PathVideos []string `json:"pathVideos"`
		PathPhotos []string `json:"pathPhotos"`
	}
	err := ctx.BindJSON(&params)
	if err != nil {
		dataResponse["status"] = 1
		dataResponse["message"] = "参数错误"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "参数错误")
		return
	}
	//获取数据库连接
	db := common.DB
	if db == nil {
		dataResponse["status"] = 9
		dataResponse["message"] = "数据库错误"
		Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "系统内部错误")
		return
	}
	//首先判断视频路径数组是否为空，如果不为空，那么就要删除视频文件和数据库中的记录
	if len(params.PathVideos) != 0 && params.PathVideos[0] != "" {
		//遍历视频路径数组，删除视频文件和数据库中的记录
		for _, pathVideo := range params.PathVideos {
			//首先删除视频文件,在alert表中找到pathVideo在params.PathVideos中的记录，然后删除
			//文件路径要处理，去掉多余的/，使用filepath.Clean()函数
			pathVideo = filepath.Clean(pathVideo)
			//删除之前要看是否存在，如果不存在，返回错误
			err = db.Where("path_video = ?", pathVideo).First(&common.Alert{}).Error
			if err != nil {
				if err == gorm.ErrRecordNotFound {
					dataResponse["status"] = 1
					dataResponse["message"] = "视频路径不存在"
					Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "视频路径不存在")
					return
				} else {
					dataResponse["status"] = 9
					dataResponse["message"] = "数据库错误 " + err.Error()
					Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "系统内部错误")
					return
				}
			}
			err = db.Unscoped().Where("path_video = ?", pathVideo).Delete(&common.Alert{}).Error //这里使用Unscoped()是为了真正删除，而不是软删除
			if err != nil {
				dataResponse["status"] = 9
				dataResponse["message"] = "数据库错误 " + err.Error()
				Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "系统内部错误")
				return
			}
			//然后删除视频文件
			err = os.Remove(pathVideo)
			if err != nil {
				dataResponse["status"] = 9
				dataResponse["message"] = "删除视频文件错误 " + err.Error()
				Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "系统内部错误")
				return
			}
		}
		//如果没有错误，那么就返回成功
		dataResponse["status"] = 0
		dataResponse["message"] = "成功"
		Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "成功")
		return
	}
	//接下来判断图片路径数组是否为空，如果不为空，那么就要删除图片文件和数据库中的记录
	if len(params.PathPhotos) != 0 && params.PathPhotos[0] != "" {
		//遍历图片路径数组，删除图片文件和数据库中的记录
		for _, pathPhoto := range params.PathPhotos {
			//首先删除图片文件,在alert表中找到pathPhoto在params.PathPhotos中的记录，然后删除
			//文件路径要处理，去掉多余的/，使用filepath.Clean()函数
			pathPhoto = filepath.Clean(pathPhoto)
			//删除之前要看是否存在，如果不存在，返回错误
			err = db.Where("path_photo = ?", pathPhoto).First(&common.Alert{}).Error
			if err != nil {
				if err == gorm.ErrRecordNotFound {
					dataResponse["status"] = 1
					dataResponse["message"] = "图片路径不存在"
					Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "图片路径不存在")
					return
				} else {
					dataResponse["status"] = 9
					dataResponse["message"] = "数据库错误 " + err.Error()
					Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "系统内部错误")
					return
				}
			}
			err = db.Unscoped().Where("path_photo = ?", pathPhoto).Delete(&common.Alert{}).Error
			if err != nil {
				dataResponse["status"] = 9
				dataResponse["message"] = "数据库错误 " + err.Error()
				Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "系统内部错误")
				return
			}
			//然后删除图片文件
			err = os.Remove(pathPhoto)
			if err != nil {
				dataResponse["status"] = 9
				dataResponse["message"] = "删除图片文件错误 " + err.Error()
				Response(ctx, http.StatusBadRequest, http.StatusBadRequest, dataResponse, "系统内部错误")
				return
			}
		}
		//如果没有错误，那么就返回成功
		dataResponse["status"] = 0
		dataResponse["message"] = "成功"
		Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "成功")
		return
	}
	//如果没有错误，那么就返回成功
	dataResponse["status"] = 0
	dataResponse["message"] = "成功"
	Response(ctx, http.StatusOK, http.StatusOK, dataResponse, "成功")
	return
}

/*
GetAlertSrc 获取报警视频
参数：pathVideo 视频路径、pathPhoto 图片路径，这两个只要不为空就会返还对应信息、但是请求只能有一个不为空，如果两个为空，那么就会返回视频路径的文件

返回:流式传递视频/图片、这里的响应就不再是用json了，也没有用Response函数
*/
// func GetAlertSrc(ctx *gin.Context) {
// 	return
// }
