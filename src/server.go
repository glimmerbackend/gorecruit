package main

import (
	"log"
	"os"
	"path/filepath"
	"server/common"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func initConfigAndLog() { //初始化配置文件和日志
	workDir, _ := os.Getwd()
	//为了每天都能更新日志文件，需要在每天的0点的时候重新创建一个日志文件，我们用一个协程来管理日志文件的创建
	//获取当前时间
	timeZone, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		log.Printf("load location error: %v", err)
	}
	today := time.Now().In(timeZone)
	nextday := time.Date(today.Year(), today.Month(), today.Day()+1, 0, 0, 0, 0, timeZone) //获取明天的时间,这里的明天是指24点
	//计算等待时间
	duration := nextday.Sub(today)
	// 启动定时器
	timer := time.NewTimer(duration)
	//开始我们的定时任务
	go func() {
		for { //这个for循环是为了让定时任务一直执行
			nowDate := time.Now().Format("2006-01-02") //获取当前日期
			//打开/创建一个日志文件，用于打印日志
			//按照日期创建日志文件，日志文件在工作目录下的src/log文件夹下,名字格式是: 2020-12-12-server.log
			fileAbsPath := workDir + "/src/log/" + nowDate + "-server.log"
			//先从fileAbsPath中得到文件夹路径，然后创建文件夹(如果文件夹不存在的话)
			fileDir := filepath.Dir(fileAbsPath)
			err := os.MkdirAll(fileDir, os.ModePerm)
			if err != nil {
				log.Printf("error creating dir: %v", err)
			}
			f, err := os.OpenFile(fileAbsPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
			if err != nil {
				log.Printf("error opening file: %v", err)
			}
			log.SetOutput(f) //设置日志输出的文件
			<-timer.C        //从timer.C中读取数据，如果没有数据的话，就会一直阻塞,从而实现定时任务
			//得到明天时间用来重置定时器
			today = time.Now().In(timeZone)
			nextday = today.AddDate(0, 0, 1)
			duration = nextday.Sub(today)
			timer.Reset(duration)
		}
	}()
	//初始化配置文件
	viper.SetConfigName("application")
	viper.SetConfigType("yml")
	viper.AddConfigPath(workDir + "/config")
	err = viper.ReadInConfig()
	if err != nil { //读取配置文件失败，打印错误日志
		log.Printf("Fatal error config file: %s \n", err)
	}
}

func main() {
	//初始化配置文件和日志
	initConfigAndLog()
	//初始化数据库
	common.InitDB()
	//设置gin的运行模式
	gin.SetMode(viper.GetString("gin.run_mode"))
	//创建一个gin引擎
	r := gin.Default()
	//收集路由
	r = CollectRoute(r)
	//运行gin引擎
	err := r.Run(":" + viper.GetString("gin.port"))
	if err != nil {
		log.Printf("gin run error: %v", err)
		return
	}
}
